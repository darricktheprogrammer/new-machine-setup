echo "" >> ~/.bashrc;  # ensure newline before making changes
echo 'PS1="\[\e[34m\][remote connection: \h] \[\e[00;31m\][\D{%F %T}] \u@\h (\w)\n:\[\e[0m\] "' >> ~/.bashrc;
echo "alias ls='ls -1FA --color=auto'" >> ~/.bashrc;
echo 'export HISTCONTROL=ignoreboth # shorthand for ignorespace and ignoredups' >> ~/.bashrc;
echo 'export HISTFILESIZE=5000' >> ~/.bashrc;

echo "" >> ~/.inputrc
echo "set completion-ignore-case on" >> ~/.inputrc
echo "set show-all-if-ambiguous on" >> ~/.inputrc
echo "TAB: menu-complete" >> ~/.inputrc

timedatectl set-timezone America/Indiana/Indianapolis

apt install curl
apt install wget
apt install micro
# apt install python3-pip

source ~/.bashrc;   # reload bashrc
bind -f ~/.inputrc  # reload inputrc

